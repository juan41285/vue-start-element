import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en'
import infiniteScroll from 'vue-infinite-scroll'
import DataTables from 'vue-data-tables'

import App from './App.vue'
import router from './router'
import VueMoment from 'vue-moment'

Vue.use(ElementUI, {locale})
Vue.use(infiniteScroll)
Vue.use(DataTables)

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})