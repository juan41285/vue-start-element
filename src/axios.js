import axios from 'axios'

export const baseUrl = `http://localhost/api-google-php`
// export const serverUrl = ``

export const HTTP = axios.create({
  baseURL: baseUrl,
  headers: {
    Authorization: 'Bearer {token}'
  }
})