import Vue from 'vue'
import VueRouter from 'vue-router'

import hwtHome from '~/components/hwtHome.vue'
import hwtSearch from '~/components/hwtSearch.vue'
import hwtAwis from '~/components/hwtAwis.vue'

Vue.use(VueRouter)

/*
 * Uncomment this section and use "load()" if you want
 * to lazy load routes.

*/
// function load (component) {
//   // '@' is aliased to src/components
//   return () => import(`@/${component}.vue`)
// }

export default new VueRouter({
  routes: [
    { path: '/', component: hwtHome, name: 'hwtHome' },
    { path: '/search', component: hwtSearch, name: 'hwtSearch' },
    { path: '/site-information', component: hwtAwis, name: 'hwtAwis' }
   
  ]
})
